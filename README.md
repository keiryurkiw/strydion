# Strydion: A from-scratch Vulkan game engine written in pure C99

Strydion is a game engine written in entirely in pure C99 that has as little
dependencies as possible.

Currently, there is only official support for Windows and Linux (X11).
Future (official) support is planned for the BSDs and MacOS and perhaps
RISC archs.

## Dependencies

### Compiletime

- Tup
- C99 compiler (no VLAs)
- Glslang

### Runtime

### General

- Vulkan

### Linux

- XCB

## Cloning

```sh
$ git clone --recurse-submodules https://gitlab.freedesktop.org/keiryurkiw/strydion
```

## Building

```sh
$ tup
```
