/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DEFINES_H
#define DEFINES_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define PI 3.141592653589793238462643383279502884197
#define PIF 3.141592653589793238462643383279502884197f

#define ONE_MIL ((uint_least32_t)1e6)
#define ONE_BIL ((uint_least32_t)1e9)

#define ONE_BYTE 8
#define ONE_KBYTE ((uint_least16_t)1 << 10)
#define ONE_MBYTE ((uint_least32_t)1 << 20)
#define ONE_GBYTE ((uint_least32_t)1 << 30)

#if defined(__clang__)
#define COMPILER_CLANG
#elif defined(__GNUC__)
#define COMPILER_GCC
#elif defined(_MSC_VER)
#define COMPILER_MSVC
#else
#define COMPILER_UNKNOWN
#endif

#endif /* DEFINES_H */
