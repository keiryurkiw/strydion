/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#define CGLM_FORCE_DEPTH_ZERO_TO_ONE
#define CGLM_FORCE_LEFT_HANDED
#include <cglm/cglm.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "defines.h"
#include "utils.h"
#include "platform/utils.h"
#include "platform/window.h"
#include "vulkan/vk_alloc.h"
#include "vulkan/vk_device.h"
#include "vulkan/vk_include.h"
#include "vulkan/vk_initializers.h"
#include "vulkan/vk_instance.h"
#include "vulkan/vk_mesh.h"
#include "vulkan/vk_pipeline.h"
#include "vulkan/vk_swapchain.h"
#include "vulkan/vk_utils.h"

#define WIDTH 800
#define HEIGHT 600

#define FRAME_OVERLAP 2
#define CUR_FRM (frames[frame_count % FRAME_OVERLAP])

/* Calculate nanoseconds per frame from frames per second. */
#define NSPF(_fps) ((int64_t)(1.0 / _fps##.0 * 1e9))

typedef struct {
	mat4 view;
	mat4 proj;
	mat4 viewproj;
} CameraData;

typedef struct {
	VkSemaphore pres_sem, rend_sem;
	VkFence rend_fen;

	VkCommandPool cmd_pool;
	VkCommandBuffer cmd_buf;

	VulkBuffer camera_buffer;
	VkDescriptorSet global_descriptor;
} FrameData;

typedef struct {
	mat4 model;
	mat4 padding;
} PushConsts;

TimeSpec delta_time = { 0 };
uint_least64_t frame_count = 0;
static FrameData frames[FRAME_OVERLAP] = { 0 };

static void
update_title_with_fps(void *win)
{
	time_t dt = ts_to_time(&delta_time);

	char fps[64];
	sprintf(fps, "Strydion | FPS: %0.0f | Frame count: %"PRIuLEAST64,
			1.0f / (dt / (float)ONE_BIL), frame_count);
	win_set_title(win, fps);
}

int
main(void)
{
	pf_init();

	VkInstance instance = vulk_init();
	if (!instance)
		return 1;

	if (!win_init())
		return 1;

	void *window = win_create(instance, WIDTH, HEIGHT);
	if (!window) {
		win_deinit();
		return 1;
	}

	VulkDevice device;
	if (!vulk_device_create(&device, instance, win_get_surface(window)))
		return 1;
	volkLoadDevice(device.handle);
	VulkAllocator allocator = vulk_alloc_init(&device);

	VkSurfaceFormatKHR surf_format = vulk_swap_get_format(&device.swap_support);
	VkRenderPass render_pass = vulk_pipe_create_renderpass(&device,
			surf_format.format);

	VulkSwapchain swapchain;
	if (!vulk_swap_create(window, &allocator, &device,
				win_get_surface(window), render_pass, &swapchain)) {
		return 1;
	}

	VkDescriptorSetLayoutBinding camera_buf_binding = {
		.binding = 0,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
	};

	VkDescriptorSetLayoutCreateInfo set_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.bindingCount = 1,
		.pBindings = &camera_buf_binding,
	};

	VkDescriptorSetLayout global_set_layout;
	vkCreateDescriptorSetLayout(device.handle,
			&set_info, NULL, &global_set_layout);

	VkDescriptorPoolSize pool_size = {
		.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = 16
	};

	VkDescriptorPoolCreateInfo pool_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.maxSets = 16,
		.poolSizeCount = 1,
		.pPoolSizes = &pool_size,
	};

	VkDescriptorPool descriptor_pool;
	vkCreateDescriptorPool(device.handle, &pool_info, NULL, &descriptor_pool);

	for (size_t i = 0; i < FRAME_OVERLAP; i++) {
		VkCommandPool cmd_pool = vulk_pipe_create_cmd_pool(&device);

		VkBufferCreateInfo buf_info = {
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = sizeof(CameraData),
			.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		};

		frames[i] = (FrameData){
			.cmd_pool = cmd_pool,
			.cmd_buf = vulk_pipe_alloc_cmd_buf(&device, cmd_pool),
			.rend_fen = vulk_pipe_create_fence(&device),
			.pres_sem = vulk_pipe_create_semaphore(&device),
			.rend_sem = vulk_pipe_create_semaphore(&device),
			.camera_buffer = vulk_alloc_buffer(&allocator, &device, &buf_info),
		};

		VkDescriptorSetAllocateInfo alloc_info = {
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
			.descriptorPool = descriptor_pool,
			.descriptorSetCount = 1,
			.pSetLayouts = &global_set_layout,
		};
		vkAllocateDescriptorSets(device.handle,
				&alloc_info, &frames[i].global_descriptor);

		VkDescriptorBufferInfo buffer_info = {
			.buffer = frames[i].camera_buffer.handle,
			.offset = 0,
			.range = sizeof(CameraData),
		};
		VkWriteDescriptorSet set_write = {
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstBinding = 0,
			.dstSet = frames[i].global_descriptor,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.pBufferInfo = &buffer_info,
		};
		vkUpdateDescriptorSets(device.handle, 1, &set_write, 0, NULL);
	}

	VulkMesh cube = vulk_mesh_create_cube(&allocator, &device);

	VkShaderModule vert = vulk_pipe_create_shader(
			&device, "shaders/simple.vert.spv");
	VkShaderModule frag = vulk_pipe_create_shader(
			&device, "shaders/simple.frag.spv");

	VkPipelineShaderStageCreateInfo shader_stage_infos[] = {
		VULK_PIPELINE_SHADER_STAGE_INFO(VK_SHADER_STAGE_VERTEX_BIT, vert),
		VULK_PIPELINE_SHADER_STAGE_INFO(VK_SHADER_STAGE_FRAGMENT_BIT, frag),
	};

	VkPipelineVertexInputStateCreateInfo vert_input_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.pVertexAttributeDescriptions = cube.input_description.attributes,
		.vertexAttributeDescriptionCount = 2,
		.pVertexBindingDescriptions = &cube.input_description.binding,
		.vertexBindingDescriptionCount = 1,
	};

	VkPipelineInputAssemblyStateCreateInfo assembly_info =
		VULK_PIPELINE_INPUT_ASSEMBLY_INFO(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

	VkViewport viewport = {
		.x = 0.0f, .y = 0.0f,
		.width = WIDTH, .height = HEIGHT,
		.minDepth = 0.0f, .maxDepth = 1.0f,
	};

	VkRect2D scissor = {
		.offset = { .x = 0, .y = 0 },
		.extent = { .width = WIDTH, .height = HEIGHT },
	};

	VkPipelineRasterizationStateCreateInfo rasterization_state_info =
		VULK_PIPELINE_RASTERIZATION_STATE_INFO(VK_POLYGON_MODE_FILL);

	VkPipelineMultisampleStateCreateInfo multisample_info =
		VULK_PIPELINE_MULTISAMPLE_STATE_INFO(VK_SAMPLE_COUNT_1_BIT);

	VkPipelineColorBlendAttachmentState color_blend_state = {
		.colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT |
			VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT,
		.blendEnable = VK_FALSE,
	};

	VkPipelineLayout layout;

	VkPushConstantRange push_constant_range = {
		.offset = 0,
		.size = sizeof(PushConsts),
		.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
	};

	VkPipelineLayoutCreateInfo layout_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pPushConstantRanges = &push_constant_range,
		.pushConstantRangeCount = 1,
		.setLayoutCount = 1,
		.pSetLayouts = &global_set_layout,
	};
	vk_oom(vkCreatePipelineLayout(device.handle, &layout_info, NULL, &layout));

	VkPipeline pipeline = vulk_pipe_create(
			&device, render_pass,
			shader_stage_infos, 2,
			vert_input_info,
			assembly_info,
			viewport, scissor,
			rasterization_state_info,
			color_blend_state,
			multisample_info,
			layout);

	win_show(window);

	uint32_t width, height, old_width, old_height;
	win_get_fb_size(window, &width, &height);
	old_width = width;
	old_height = height;
	VkResult ret;
	while (!win_should_close(window)) {
		win_poll_events();

		win_get_fb_size(window, &width, &height);
		if (old_width != width || old_height != height) {
			vulk_swap_recreate(window, &allocator, &device,
				win_get_surface(window), render_pass, &swapchain);
			old_width = width;
			old_height = height;
		}

		uint32_t img_idx;
		ret = vkAcquireNextImageKHR(device.handle, swapchain.handle, ONE_BIL,
				CUR_FRM.pres_sem, VK_NULL_HANDLE, &img_idx);
		vk_oom(ret);
		if (ret == VK_ERROR_OUT_OF_DATE_KHR) {
			vulk_swap_recreate(window, &allocator, &device,
				win_get_surface(window), render_pass, &swapchain);
		} else if (ret != VK_SUBOPTIMAL_KHR) {
			VK_ASSERT(ret);
		}

		vk_oom(vkResetFences(device.handle, 1, &CUR_FRM.rend_fen));

		VkCommandBufferBeginInfo begin_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
		};
		vk_oom(vkBeginCommandBuffer(CUR_FRM.cmd_buf, &begin_info));

		VkClearValue cvs[] = {
			{ .color = { .float32 = { 0.0f, 0.0f, 0.0f, 0.0f } } },
			{ .depthStencil = { .depth = 1.0f } },
		};

		VkRenderPassBeginInfo rp_info = {
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.renderPass = render_pass,
			.renderArea = (VkRect2D){
				.extent = swapchain.extent,
			},
			.framebuffer = swapchain.objects.framebuffers[img_idx],
			.pClearValues = cvs,
			.clearValueCount = 2,
		};

		time_t time = get_time();

		mat4 proj;
		glm_perspective(PIF/2.0f, width / (float)height, 0.1f, 100.0f, proj);

		mat4 view = GLM_MAT4_IDENTITY_INIT;
		glm_translate_z(view, 4.0f);

		mat4 viewproj;
		glm_mat4_mul(proj, view, viewproj);

		PushConsts consts = { .model = GLM_MAT4_IDENTITY_INIT };
		glm_rotate_x(consts.model, sinf(time / 1e9f), consts.model);
		glm_rotate_y(consts.model, time / 1e9f, consts.model);

		void *data;
		vkMapMemory(device.handle, CUR_FRM.camera_buffer.memory,
				0, sizeof(CameraData), 0, &data);
		uintptr_t cam_ptr = (uintptr_t)data;
		memcpy((void *)(cam_ptr + offsetof(CameraData, proj)),
				proj, sizeof(proj));
		memcpy((void *)(cam_ptr + offsetof(CameraData, view)),
				view, sizeof(view));
		memcpy((void *)(cam_ptr + offsetof(CameraData, viewproj)),
				viewproj, sizeof(viewproj));
		vkUnmapMemory(device.handle, CUR_FRM.camera_buffer.memory);

		vkCmdBeginRenderPass(CUR_FRM.cmd_buf, &rp_info,
				VK_SUBPASS_CONTENTS_INLINE);

		viewport.width = scissor.extent.width = width;
		viewport.height = scissor.extent.height = height;
		vkCmdSetViewport(CUR_FRM.cmd_buf, 0, 1, &viewport);
		vkCmdSetScissor(CUR_FRM.cmd_buf, 0, 1, &scissor);

		vkCmdBindPipeline(CUR_FRM.cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS,
				pipeline);
		vkCmdBindDescriptorSets(CUR_FRM.cmd_buf,
				VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1,
				&CUR_FRM.global_descriptor, 0, NULL);
		vkCmdBindVertexBuffers(CUR_FRM.cmd_buf, 0, 1,
				&cube.vertex_buffer.handle, &(VkDeviceSize){ 0 });
		vkCmdPushConstants(CUR_FRM.cmd_buf, layout, VK_SHADER_STAGE_VERTEX_BIT,
				0, sizeof(PushConsts), &consts);
		vkCmdDraw(CUR_FRM.cmd_buf, cube.vertex_count, 1, 0, 0);

		vkCmdEndRenderPass(CUR_FRM.cmd_buf);

		ret = vkEndCommandBuffer(CUR_FRM.cmd_buf);
		vk_oom(ret);
		VK_ASSERT(ret);

		VkPipelineStageFlags wait_stage =
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSubmitInfo submit = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pWaitDstStageMask = &wait_stage,
			.pWaitSemaphores = &CUR_FRM.pres_sem,
			.waitSemaphoreCount = 1,
			.pSignalSemaphores = &CUR_FRM.rend_sem,
			.signalSemaphoreCount = 1,
			.commandBufferCount = 1,
			.pCommandBuffers = &CUR_FRM.cmd_buf,
		};

		ret = vkQueueSubmit(device.gfx_queue, 1, &submit, CUR_FRM.rend_fen);
		vk_oom(ret);
		VK_ASSERT(ret);

		VkPresentInfoKHR present_info = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pSwapchains = &swapchain.handle,
			.swapchainCount = 1,
			.pWaitSemaphores = &CUR_FRM.rend_sem,
			.waitSemaphoreCount = 1,
			.pImageIndices = &img_idx,
		};

		ret = vkQueuePresentKHR(device.gfx_queue, &present_info);
		vk_oom(ret);
		if (ret == VK_SUBOPTIMAL_KHR || ret == VK_ERROR_OUT_OF_DATE_KHR) {
			vulk_swap_recreate(window, &allocator, &device,
				win_get_surface(window), render_pass, &swapchain);
		} else {
			VK_ASSERT(ret);
		}

		ret = vkWaitForFences(device.handle, 1, &CUR_FRM.rend_fen,
				VK_TRUE, ONE_BIL);
		vk_oom(ret);
		VK_ASSERT(ret);

		vk_oom(vkResetCommandBuffer(CUR_FRM.cmd_buf, 0));

		delta_time = strangle(NSPF(60));
		++frame_count;
		update_title_with_fps(window);
	}

	vkDeviceWaitIdle(device.handle);

	vkDestroyPipeline(device.handle, pipeline, NULL);
	vkDestroyPipelineLayout(device.handle, layout, NULL);

	vkDestroyShaderModule(device.handle, vert, NULL);
	vkDestroyShaderModule(device.handle, frag, NULL);

	vkDestroyDescriptorSetLayout(device.handle, global_set_layout, NULL);
	vkDestroyDescriptorPool(device.handle, descriptor_pool, NULL);

	for (size_t i = 0; i < FRAME_OVERLAP; i++) {
		vkDestroySemaphore(device.handle, frames[i].rend_sem, NULL);
		vkDestroySemaphore(device.handle, frames[i].pres_sem, NULL);
		vkDestroyFence(device.handle, frames[i].rend_fen, NULL);

		vkDestroyCommandPool(device.handle, frames[i].cmd_pool, NULL);
		vulk_alloc_free_buffer(&device, &frames[i].camera_buffer);
	}

	vkDestroyRenderPass(device.handle, render_pass, NULL);

	vulk_mesh_destroy(&device, &cube);

	vulk_swap_destroy(&device, &swapchain);
	vulk_device_destroy(&device);

	win_destroy(instance, window);
	win_deinit();

	vulk_deinit(instance);

	pf_deinit();

	return 0;
}
