/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "../defines.h"
#include "../utils.h"
#include "utils.h"

extern inline TimeSpec time_to_ts(time_t time);
extern inline time_t ts_to_time(TimeSpec *ts);
extern inline void ts_sub(TimeSpec *restrict a, TimeSpec *restrict b,
		TimeSpec *restrict out);

/* Unused here */
bool pf_init(void) { return true; }
void pf_deinit(void) {  }

void
get_ts_time(TimeSpec *ts)
{
	clock_gettime(CLOCK_MONOTONIC, ts);
}

time_t
get_time(void)
{
	TimeSpec ts;
	get_ts_time(&ts);
	return ts_to_time(&ts);
}

void
pf_sleep(TimeSpec *ts)
{
	clock_nanosleep(CLOCK_MONOTONIC, 0, ts, NULL);
}

void *
mem_alloc(size_t size)
{
	assert(size > 0 && "Tried allocating too little memory");

	void *ptr = malloc(size);
	if (!ptr) {
		perr("Failed to allocate %zu bytes: %s", size, strerror(errno));
		abort();
	}

	return ptr;
}

void *
mem_zalloc(size_t size)
{
	void *ptr = mem_alloc(size);
	memset(ptr, 0x00, size);
	return ptr;
}

void *
mem_realloc(void *ptr, size_t size)
{
	assert(size > 0 && "Tried reallocating too little memory");

	ptr = realloc(ptr, size);
	if (!ptr) {
		perr("Failed to allocate %zu bytes: %s", size, strerror(errno));
		abort();
	}

	return ptr;
}

void
mem_free(void *ptr)
{
	free(ptr);
}

File
map_file(char *path)
{
	assert(path != NULL);

	int fd = open(path, O_RDONLY);
	if (fd < 0) {
		perr("Failed to open file '%s': %s", path, strerror(errno));
		return (File){ 0 };
	}

	struct stat sb;
	if (fstat(fd, &sb) < 0) {
		perr("Failed to get status of file '%s': %s", path, strerror(errno));
		return (File){ 0 };
	} else if (sb.st_size == 0) {
		perr("Cannot map file '%s' with a size of zero", path);
		close(fd);
		return (File){ 0 };
	}

	void *data = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED) {
		perr("Failed to map file '%s': %s", path, strerror(errno));
		close(fd);
		return (File){ 0 };
	}

	close(fd);

	return (File){ .data = data, .size = sb.st_size };
}

void
unmap_file(File *file)
{
	int ret = munmap(file->data, file->size);
	assert(ret == 0 && "Invalid arguments passed to munmap");
}
