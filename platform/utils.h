/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Nalanus.
 *
 * Nalanus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nalanus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nalanus.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PLATFORM_UTILS_H
#define PLATFORM_UTILS_H

#include <time.h>

#include "../defines.h"

typedef struct {
	void *data;
	size_t size;
} File;

#ifdef OS_win32
typedef struct {
	time_t tv_sec;
	long long tv_nsec;
} TimeSpec;
#else
typedef struct timespec TimeSpec;
#endif

#ifdef OS_win32
void win32_perr(char *fmt, ...);
#endif

bool pf_init(void);
void pf_deinit(void);

void get_ts_time(TimeSpec *ts);
time_t get_time(void);

void pf_sleep(TimeSpec *ts);

void *mem_alloc(size_t size);
void *mem_zalloc(size_t size);
void *mem_realloc(void *ptr, size_t size);
void mem_free(void *ptr);

File map_file(char *path);
void unmap_file(File *file);

inline void
ts_sub(TimeSpec *restrict a, TimeSpec *restrict b, TimeSpec *restrict out)
{
	out->tv_sec = a->tv_sec - b->tv_sec;
	out->tv_nsec = a->tv_nsec - b->tv_nsec;

	if (out->tv_nsec < 0) {
		--out->tv_sec;
		out->tv_nsec += ONE_BIL;
	}
}

inline TimeSpec
time_to_ts(time_t time)
{
	return (TimeSpec){
		.tv_sec = time / ONE_BIL,
		.tv_nsec = time % ONE_BIL,
	};
}

inline time_t
ts_to_time(TimeSpec *ts)
{
	return ts->tv_sec*ONE_BIL + ts->tv_nsec;
}

#endif /* PLATFORM_UTILS_H */
