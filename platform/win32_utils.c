/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#include <windows.h>

#include "../defines.h"
#include "../utils.h"
#include "utils.h"

HINSTANCE hinstance = INVALID_HANDLE_VALUE;

static LARGE_INTEGER clock_frequency = { 0 };
static HANDLE memory_heap = INVALID_HANDLE_VALUE;
static HANDLE frame_timer = INVALID_HANDLE_VALUE;

extern inline TimeSpec time_to_ts(time_t time);
extern inline time_t ts_to_time(TimeSpec *ts);
extern inline void ts_sub(TimeSpec *restrict a, TimeSpec *restrict b,
		TimeSpec *restrict out);

void
win32_perr(char *fmt, ...)
{
	fputs("nalanus: \x1b[01;31merror: \x1b[0m", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	DWORD err_code = GetLastError();
	if (err_code == ERROR_SUCCESS) {
		fputc('\n', stderr);
		return;
	}

	LPTSTR buf;
	DWORD ret = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, err_code,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&buf, 0, NULL);
	if (ret > 0) {
		/* Windows adds a newline to the end of buf. */
		fprintf(stderr, ": %s", buf);
		LocalFree(buf);
	} else {
		fputc('\n', stderr);
	}
}

bool
pf_init(void)
{
	hinstance = GetModuleHandleA(NULL);

	QueryPerformanceFrequency(&clock_frequency);

	/* NOTE: Removing serialization also removes thread safety.  This MUST
	 * be removed for multithreading. */
	memory_heap = HeapCreate(HEAP_NO_SERIALIZE, 0, 0);
	if (!memory_heap) {
		win32_perr("Failed to create memory heap");
		return false;
	}

	frame_timer = CreateWaitableTimerW(NULL, FALSE, NULL);
	if (!frame_timer) {
		HeapDestroy(memory_heap);
		win32_perr("Failed to create timer");
		return false;
	}

	return true;
}

void
pf_deinit(void)
{
	CloseHandle(frame_timer);
	HeapDestroy(memory_heap);
}

void
get_ts_time(TimeSpec *ts)
{
	assert(clock_frequency.QuadPart &&
			"get_ts_time() has been called before pf_init()");

	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	ts->tv_sec = now.QuadPart / clock_frequency.QuadPart;
	ts->tv_nsec = now.QuadPart % clock_frequency.QuadPart;
	ts->tv_nsec *= ONE_BIL / clock_frequency.QuadPart;
}

time_t
get_time(void)
{
	assert(clock_frequency.QuadPart &&
			"get_time() has been called before pf_init()");

	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	time_t sec = now.QuadPart / clock_frequency.QuadPart;
	time_t nsec = now.QuadPart % clock_frequency.QuadPart;
	nsec *= ONE_BIL / clock_frequency.QuadPart;

	return sec*ONE_BIL + nsec;
}

void
pf_sleep(TimeSpec *ts)
{
	static time_t estimate = 5 * ONE_MIL;
	static time_t mean = 5 * ONE_MIL;
	static time_t m2 = 0;
	static int64_t count= 1;

	time_t ns = ts_to_time(ts);
	while (ns > estimate) {
		TimeSpec start, end, res;

		time_t to_wait = ns - estimate;
		LARGE_INTEGER due = { .QuadPart = -(to_wait / 100) };
		get_ts_time(&start);
		SetWaitableTimer(frame_timer, &due, 0, NULL, NULL, FALSE);
		WaitForSingleObject(frame_timer, INFINITE);
		get_ts_time(&end);

		ts_sub(&end, &start, &res);
		time_t observed = ts_to_time(&res);
		ns -= observed;

		++count;
		time_t delta = observed - mean;
		mean += delta / count;
		m2 += delta * (observed - mean);
		time_t stddev = sqrtf(m2 / count - 1);
		estimate = mean + stddev / 5;
	}

	/* Spin-lock */
	time_t start = get_time();
	while (get_time() - start < ns)
		;
}

static inline void *
internal_alloc(SIZE_T size, DWORD flags)
{
	assert(memory_heap && "mem_alloc() has been called before pf_init()");
	assert(size != 0 && "Tried allocating too little memory");

	void *ptr = HeapAlloc(memory_heap, flags, size);
	if (!ptr) {
		perr("Failed to allocate %zu bytes", size);
		abort();
	}

	return ptr;
}

void *
mem_alloc(size_t size)
{
	return internal_alloc(size, 0);
}

void *
mem_zalloc(size_t size)
{
	return internal_alloc(size, HEAP_ZERO_MEMORY);
}

void *
mem_realloc(void *ptr, size_t size)
{
	assert(memory_heap && "mem_realloc() has been called before pf_init()");
	assert(size != 0 && "Tried reallocating too little memory");

	if (!ptr)
		return mem_alloc(size);

	void *new_ptr = HeapReAlloc(memory_heap, 0, ptr, size);
	if (!new_ptr) {
		perr("Failed to allocate %zu bytes", size);
		abort();
	}

	return new_ptr;
}

void
mem_free(void *ptr)
{
	assert(memory_heap && "mem_free() has been called before pf_init()");

	HeapFree(memory_heap, 0, ptr);
}

File
map_file(char *path)
{
	HANDLE handle = CreateFileA(path, GENERIC_READ,
			FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (handle == INVALID_HANDLE_VALUE) {
		win32_perr("Failed to open file '%s'", path);
		return (File){ 0 };
	}

	LARGE_INTEGER size;
	BOOL ret = GetFileSizeEx(handle, &size);
	if (!ret) {
		win32_perr("Failed to get size of file '%s'", path);
		CloseHandle(handle);
		return (File){ 0 };
	} else if (size.QuadPart == 0) {
		perr("Cannot map file '%s' which has a size of 0", path);
		CloseHandle(handle);
		return (File){ 0 };
	}

	HANDLE map = CreateFileMappingA(handle, NULL, PAGE_READONLY, 0, 0, NULL);
	if (!map) {
		win32_perr("Failed to map file '%s'", path);
		CloseHandle(handle);
		return (File){ 0 };
	}

	LPVOID data = MapViewOfFile(map, FILE_MAP_READ, 0, 0, size.QuadPart);
	CloseHandle(map);
	CloseHandle(handle);
	if (!data) {
		win32_perr("Failed to map view of file '%s'", path);
		return (File){ 0 };
	}

	return (File){ .data = data, .size = size.QuadPart };
}

void
unmap_file(File *file)
{
	BOOL ret = UnmapViewOfFile(file->data);
	assert(ret && "Invalid parameters passed to unmap_file()");
}
