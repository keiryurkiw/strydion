/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NORASTEROPS
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
/* windows.h is included before vk_include.h because of
 * the above 'NO' macro shenanigans. */
#include <windows.h>
#define VK_INCLUDE_PLATFORM
#include "../vulkan/vk_include.h"

#include "../defines.h"
#include "../utils.h"
#include "../vulkan/vk_utils.h"
#include "utils.h"
#include "window.h"

#define WCAST(_win) ((Win32Window *)_win)

typedef union {
	ATOM atom;
	LPCTSTR lpctstr;
} WindowClass;

typedef struct Win32Window {
	HWND handle;
	WindowClass class;
	VkSurfaceKHR surface;

	bool should_close;

	struct Win32Window *prev, *next;
} Win32Window;

static Win32Window *win_list_head = NULL;

extern HINSTANCE hinstance;

static LRESULT CALLBACK process_message(HWND handle, UINT msg,
		WPARAM wparam, LPARAM lparam);

/* Unused here */
bool win_init(void) { return true; }
void win_deinit(void) { }

static inline void
insert_win(Win32Window *win)
{
	win->prev = NULL;
	win->next = win_list_head;

	if (win_list_head != NULL)
		win_list_head->prev = win;
	win_list_head = win;
}

static inline void
remove_win(Win32Window *win)
{
	if (win_list_head == win)
		win_list_head = win->next;

	if (win->next != NULL)
		win->next->prev = win->prev;
	if (win->prev != NULL)
		win->prev->next = win->next;
}

static Win32Window *
find_win(HWND handle)
{
	/* This /should/ be safe because find_win() shouldn't ever be called with a
	 * handle which is not in the list of windows. */
	Win32Window *curr = win_list_head;
	while (curr->handle != handle)
		curr = curr->next;
	return curr;
}

void *
win_create(VkInstance vk_inst, uint32_t width, uint32_t height)
{
	assert(hinstance != INVALID_HANDLE_VALUE &&
			"Did not call pf_init() before win_create()");

	WindowClass class = { 0 };

	char class_name[32];
	static unsigned long long windows_created = 0;
	sprintf(class_name, "strydion_window_class-%llu", ++windows_created);

	WNDCLASSEXA wc = {
		.cbSize = sizeof(WNDCLASSEXA),
		.style = CS_DBLCLKS, /* Get double-clicks */
		.lpfnWndProc = process_message,
		.hInstance = hinstance,
		.hCursor = LoadImageA(
				NULL,
				MAKEINTRESOURCEA(IDC_ARROW),
				IMAGE_CURSOR,
				LR_DEFAULTSIZE, LR_DEFAULTSIZE,
				LR_SHARED),
		.lpszClassName = class_name,
	};
	if (!(class.atom = RegisterClassExA(&wc))) {
		win32_perr("Failed to register window");
		return NULL;
	}

	DWORD ex_style = WS_EX_APPWINDOW;
	DWORD style =
		WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU |
		WS_SIZEBOX | WS_MAXIMIZEBOX | WS_MINIMIZEBOX;

	RECT border = { 0 };
	AdjustWindowRectEx(&border, style, FALSE, ex_style);
	width += border.right - border.left;
	height += border.bottom - border.top;

	HANDLE handle = CreateWindowExA(
			ex_style,
			class.lpctstr,
			"Strydion",
			style,
			CW_USEDEFAULT, CW_USEDEFAULT, /* Window position */
			width, height,
			NULL, NULL, /* Parent window and menu respectively */
			hinstance, NULL);
	if (!handle) {
		win32_perr("Failed to create window");
		UnregisterClassA(class.lpctstr, hinstance);
		return NULL;
	}

	VkWin32SurfaceCreateInfoKHR info = {
		.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
		.hinstance = hinstance,
		.hwnd = handle,
	};

	VkSurfaceKHR surface;
	vk_oom(vkCreateWin32SurfaceKHR(vk_inst, &info, NULL, &surface));

	Win32Window *win32_win = mem_zalloc(sizeof(*win32_win));
	win32_win->handle = handle;
	win32_win->class = class;
	win32_win->surface = surface;
	insert_win(win32_win);

	return win32_win;
}

void
win_destroy(VkInstance vk_inst, void *win)
{
	vkDestroySurfaceKHR(vk_inst, WCAST(win)->surface, NULL);

	DestroyWindow(WCAST(win)->handle);
	BOOL ret = UnregisterClassA(WCAST(win)->class.lpctstr, hinstance);
	assert(ret && "Window class atom is invalid");

	remove_win(win);
	mem_free(win);
}

void
win_set_title(void *win, char *title)
{
	SetWindowTextA(WCAST(win)->handle, title);
}

void
win_show(void *win)
{
	ShowWindow(WCAST(win)->handle, SW_SHOW);
}

void
win_hide(void *win)
{
	ShowWindow(WCAST(win)->handle, SW_HIDE);
}

void
win_get_fb_size(void *win, uint32_t *restrict width, uint32_t *restrict height)
{
	RECT area;
	GetClientRect(WCAST(win)->handle, &area);
	*width = area.right;
	*height = area.bottom;
}

static LRESULT CALLBACK
process_message(HWND handle, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
		case WM_SYSCOMMAND:
			/* Ignore menu events because there isn't a menu */
			if (wparam == SC_KEYMENU)
				return 0;
			break;
		case WM_ERASEBKGND:
			return 1;
		case WM_CLOSE:
			find_win(handle)->should_close = true;
			return 0;
	}

	return DefWindowProcA(handle, msg, wparam, lparam);
}

void
win_poll_events(void)
{
	MSG msg;
	while (PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

bool
win_should_close(void *win)
{
	return WCAST(win)->should_close;
}

VkSurfaceKHR
win_get_surface(void *win)
{
	return WCAST(win)->surface;
}
