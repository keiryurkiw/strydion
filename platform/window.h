/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WINDOW_H
#define WINDOW_H

#include "../vulkan/vk_include.h"

#include "../defines.h"

bool win_init(void);
void win_deinit(void);

void *win_create(VkInstance vk_inst, uint32_t width, uint32_t height);
void win_destroy(VkInstance vk_inst, void *win);

void win_set_title(void *win, char *title);

void win_show(void *win);
void win_hide(void *win);

void win_get_fb_size(void *win, uint32_t *restrict width,
		uint32_t *restrict height);

void win_poll_events(void);
bool win_should_close(void *win);

VkSurfaceKHR win_get_surface(void *win);

#endif /* WINDOW_H */
