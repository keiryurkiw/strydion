#version 460

layout (location = 0) out vec4 frag_colour;

layout (location = 0) in vec3 colour;

void main()
{
	frag_colour = vec4(colour, 1.0f);
}
