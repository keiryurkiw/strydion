#version 460

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 clr;

layout (location = 0) out vec3 colour;

layout (set = 0, binding = 0) uniform CameraBuffer {
	mat4 view;
	mat4 proj;
	mat4 viewproj;
} camera_data;

layout (push_constant) uniform constants
{
	mat4 model;
	mat4 padding;
} PushConsts;

void main()
{
	gl_Position = camera_data.viewproj * PushConsts.model * vec4(pos, 1.0f);
	colour = clr;
}
