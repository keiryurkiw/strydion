/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "defines.h"
#include "utils.h"
#include "platform/utils.h"

extern inline size_t round_up_p2(size_t n, size_t p);
extern inline size_t round_down_p2(size_t n, size_t p);

/* perr() and pwrn() have a lot of duplicate code, but I don't have a good
 * way of coalescing them. */
void
perr(char *fmt, ...)
{
	fputs("strydion: \x1b[01;31merror:\x1b[0m ", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);
}

void
pwrn(char *fmt, ...)
{
	fputs("strydion: \x1b[01;33mwarning:\x1b[0m ", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);
}

static inline time_t
elapsed_time(time_t time)
{
	return get_time() - time;
}

/* Based off of libstrangle. */
TimeSpec
strangle(time_t target_frametime)
{
	static time_t old_time = 0;
   	static time_t overhead = 0;

	time_t time, start;
	time = start = get_time();

	time_t sleep_time = target_frametime - elapsed_time(old_time);
	if (sleep_time > overhead) {
		TimeSpec ts = time_to_ts(sleep_time -= overhead);
		pf_sleep(&ts);

		time = get_time();
		overhead = (time-start - sleep_time + overhead*99) / 100;
	} else {
		time = get_time();
	}

	TimeSpec ret = { .tv_nsec = time - old_time };

	old_time = time;

	return ret;
}

char *
basename(char *path)
{
	if (!path || *path == '\0')
		return ".";

	char *slash = strrchr(path, '/');
	if (!slash)
		return path;

	if (*(slash + 1) == '\0') {
		while (slash != path && *slash == '/')
			--slash;
		*(slash + 1) = '\0';
	}

	slash = strrchr(path, '/');
	if (!slash)
		return path;

	if (*(slash + 1) != '\0')
		++slash;

	return slash;
}

char *
extname(char *path)
{
	if (!path || *path == '\0')
		return ".";

	char *slash = strrchr(path, '/');
	if (slash)
		path = slash + 1;

	char *dot = strchr(path, '.');
	return dot ? dot + 1 : ".";
}
