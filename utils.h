/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#include <time.h>

#include "defines.h"
#include "platform/utils.h"

#define MAX(_a, _b) (_a > _b ? _a : _b)
#define MIN(_a, _b) (_a < _b ? _a : _b)
#define CLAMP(_x, _min, _max) (_x > _max ? _max : (_x < _min ? _min : _x))

#define RET_CASE_STR(_case) case _case: return #_case
/* Get length of a string literal */
#define LIT_LEN(_lit) (sizeof(_lit) / sizeof(*_lit) - 1)
/* Get length of a stack array */
#define ARR_LEN(_arr) (sizeof(_arr) / sizeof(*_arr))

void perr(char *fmt, ...);
void pwrn(char *fmt, ...);

TimeSpec strangle(time_t target_frametime);

/* A POSIX-compliant implementation of basename() and extname.
 * NOTE: path may be modified. */
char *basename(char *path);
char *extname(char *path);

/* Round up to a power of 2 */
inline size_t
round_up_p2(size_t n, size_t p)
{
	/* Make sure p is a power of 2 */
	assert((p != 0) && ((p & (p - 1)) == 0) && "p is not a power of 2");
	return (n + p - 1) & ~(p - 1);
}

/* Round down to a power of 2 */
inline size_t
round_down_p2(size_t n, size_t p)
{
	/* Make sure p is a power of 2 */
	assert((p != 0) && ((p & (p - 1)) == 0) && "p is not a power of 2");
	return n & ~(n - 1);
}

#endif /* UTILS_H */
