/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

/* For now I'll just use raw Vulkan allocations, but in the future I'm going
 * to use a custom TLSF allocator. */

#include "vk_include.h"

#include "../defines.h"
#include "vk_alloc.h"
#include "vk_device.h"
#include "vk_utils.h"

VulkAllocator
vulk_alloc_init(VulkDevice *device)
{
	VulkAllocator allocator;
	vkGetPhysicalDeviceMemoryProperties(device->physical, &allocator.mem_props);
	return allocator;
}

static uint32_t
find_props(VkPhysicalDeviceMemoryProperties *mem_props, uint32_t type_bits,
		VkMemoryPropertyFlags prop_flags)
{
	uint32_t count = mem_props->memoryTypeCount;
	for (uint32_t i = 0; i < count; i++) {
		bool is_required_type = type_bits & ((uint32_t)1 << i);

		VkMemoryPropertyFlags props = mem_props->memoryTypes[i].propertyFlags;
		bool has_required_props = (props & prop_flags) == prop_flags;

		if (is_required_type && has_required_props)
			return i;
	}

	return UINT32_MAX;
}

VulkBuffer
vulk_alloc_buffer(VulkAllocator *allocator, VulkDevice *device,
		VkBufferCreateInfo *buffer_info)
{
	VkBuffer buffer;
	vk_oom(vkCreateBuffer(device->handle, buffer_info, NULL, &buffer));

	VkMemoryRequirements requirements;
	vkGetBufferMemoryRequirements(device->handle, buffer, &requirements);

	VkMemoryPropertyFlags flags =
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	uint32_t type = find_props(&allocator->mem_props,
			requirements.memoryTypeBits, flags);
	if (type == UINT32_MAX) {
		flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		type = find_props(&allocator->mem_props,
				requirements.memoryTypeBits, flags);
		if (type == UINT32_MAX)
			return (VulkBuffer){ 0 };
	}

	VkMemoryAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = requirements.size,
		.memoryTypeIndex = type,
	};

	VkDeviceMemory memory;
	VkResult ret = vkAllocateMemory(device->handle, &alloc_info, NULL, &memory);
	vk_oom(ret);
	VK_ASSERT(ret);

	vkBindBufferMemory(device->handle, buffer, memory, 0);

	return (VulkBuffer){
		.handle = buffer,
		.memory = memory,
		.size = requirements.size,
	};
}

void
vulk_alloc_free_buffer(VulkDevice *device, VulkBuffer *buffer)
{
	vkDestroyBuffer(device->handle, buffer->handle, NULL);
	vkFreeMemory(device->handle, buffer->memory, NULL);
}

VulkImage
vulk_alloc_image(VulkAllocator *allocator, VulkDevice *device,
		VkImageCreateInfo *image_info)
{
	VkImage image;
	vk_oom(vkCreateImage(device->handle, image_info, NULL, &image));

	VkMemoryRequirements requirements;
	vkGetImageMemoryRequirements(device->handle, image, &requirements);

	VkMemoryPropertyFlags flags =
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	uint32_t type = find_props(&allocator->mem_props,
			requirements.memoryTypeBits, flags);
	if (type == UINT32_MAX)
		return (VulkImage){ 0 };

	VkMemoryAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = requirements.size,
		.memoryTypeIndex = type,
	};

	VkDeviceMemory memory;
	VkResult ret = vkAllocateMemory(device->handle, &alloc_info, NULL, &memory);
	vk_oom(ret);
	VK_ASSERT(ret);

	vkBindImageMemory(device->handle, image, memory, 0);

	return (VulkImage){
		.handle = image,
		.memory = memory,
		.size = requirements.size,
	};
}

void
vulk_alloc_free_image(VulkDevice *device, VulkImage *image)
{
	vkDestroyImage(device->handle, image->handle, NULL);
	vkFreeMemory(device->handle, image->memory, NULL);
}
