/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_ALLOC_H
#define VK_ALLOC_H

#include "../defines.h"
#include "vk_device.h"

typedef struct {
	VkPhysicalDeviceMemoryProperties mem_props;
} VulkAllocator;

typedef struct {
	VkBuffer handle;
	VkDeviceMemory memory;
	VkDeviceSize size;
} VulkBuffer;

typedef struct {
	VkImage handle;
	VkDeviceMemory memory;
	VkDeviceSize size;
} VulkImage;

VulkAllocator vulk_alloc_init(VulkDevice *device);

VulkBuffer vulk_alloc_buffer(VulkAllocator *allocator, VulkDevice *device,
		VkBufferCreateInfo *buffer_info);
void vulk_alloc_free_buffer(VulkDevice *device, VulkBuffer *buffer);

VulkImage vulk_alloc_image(VulkAllocator *allocator, VulkDevice *device,
		VkImageCreateInfo *image_info);
void vulk_alloc_free_image(VulkDevice *device, VulkImage *image);

#endif /* VK_ALLOC_H */
