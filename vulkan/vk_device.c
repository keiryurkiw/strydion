/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <string.h>
#include "vk_include.h"

#include "../defines.h"
#include "../utils.h"
#include "../platform/utils.h"
#include "vk_device.h"
#include "vk_swapchain.h"
#include "vk_utils.h"

typedef struct {
	bool has_gfx, has_prs, has_cmp, has_xfr;
	bool discrete;
	const char *const *exts;
	int ext_count;
} DeviceFeatures;

static const DeviceFeatures required_features = {
	.has_gfx = true,
	.has_prs = true,
	.exts = (const char *const[]){ VK_KHR_SWAPCHAIN_EXTENSION_NAME },
	.ext_count = 1,
};

VulkSwapSupport
vulk_device_query_swap_supp(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t fmt_count, mode_count;

	vk_oom(vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface,
				&fmt_count, NULL));
	if (fmt_count == 0)
		return (VulkSwapSupport){ 0 };

	vk_oom(vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
				&mode_count, NULL));
	if (mode_count == 0)
		return (VulkSwapSupport){ 0 };

	VkSurfaceFormatKHR *formats = mem_alloc(fmt_count * sizeof(*formats));
	vk_oom(vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface,
				&fmt_count, formats));

	VkPresentModeKHR *modes = mem_alloc(mode_count * sizeof(*modes));
	vk_oom(vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
				&mode_count, modes));

	VkSurfaceCapabilitiesKHR caps;
	vk_oom(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &caps));

	return (VulkSwapSupport){
		.capabilities = caps,
		.formats = formats,
		.format_count = fmt_count,
		.pres_modes = modes,
		.pres_mode_count = mode_count,
	};
}

VulkQFamilyIndices
vulk_device_get_fam_indices(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t count;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, NULL);
	VkQueueFamilyProperties *fam_props = mem_alloc(count * sizeof(*fam_props));
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, fam_props);

	VulkQFamilyIndices indices;
	memset(&indices, 0xFF, sizeof(indices));

	int min_trans_count = INT_MAX; /* Try to get a dedicated transfer queue. */
	for (size_t i = 0; i < count; i++) {
		int trans_count = 0;

		if (fam_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.gfx = i;
			++trans_count;
		}
		if (fam_props[i].queueFlags & VK_QUEUE_COMPUTE_BIT) {
			indices.cmp = i;
			++trans_count;
		}
		if (fam_props[i].queueFlags & VK_QUEUE_TRANSFER_BIT) {
			if (trans_count <= min_trans_count) {
				min_trans_count = trans_count;
				indices.xfr = i;
			}
		}

		VkBool32 supported;
		vk_oom(vkGetPhysicalDeviceSurfaceSupportKHR(device, i,
					surface, &supported));
		if (supported)
			indices.prs = i;
	}

	mem_free(fam_props);

	return indices;
}

static bool
exts_are_supported(VkPhysicalDevice device, const char *const *exts,
		unsigned int ext_count)
{
	uint32_t count;
	vk_oom(vkEnumerateDeviceExtensionProperties(device, NULL, &count, NULL));
	if (count == 0) {
		/* Return true if we didn't request any extensions,
		 * otherwise return false. */
		return ext_count == 0;
	}
	VkExtensionProperties *props = mem_alloc(count * sizeof(*props));
	vk_oom(vkEnumerateDeviceExtensionProperties(device, NULL, &count, props));

	for (size_t i = 0; i < ext_count; i++) {
		bool found = false;
		for (size_t j = 0; j < count; j++) {
			if (strcmp(exts[i], props[j].extensionName) == 0) {
				found = true;
				break;
			}
		}

		if (!found)
			return false;
	}

	mem_free(props);

	return true;
}

static bool
device_meets_requirements(VkPhysicalDevice device, VkSurfaceKHR surface,
		DeviceFeatures requirements, VulkDevice *vulkan_device)
{
	VulkQFamilyIndices indices = vulk_device_get_fam_indices(device, surface);
	if ((requirements.has_gfx && indices.gfx == UINT32_MAX) ||
			(requirements.has_prs && indices.prs == UINT32_MAX) ||
			(requirements.has_cmp && indices.cmp == UINT32_MAX) ||
			(requirements.has_xfr && indices.xfr == UINT32_MAX)) {
		return false;
	}

	VulkSwapSupport swap_support = vulk_device_query_swap_supp(device, surface);
	if (swap_support.format_count == 0 || swap_support.pres_mode_count == 0)
		return false;

	if (!exts_are_supported(device, required_features.exts,
				required_features.ext_count)) {
		mem_free(swap_support.formats);
		mem_free(swap_support.pres_modes);
		return false;
	}

	vulkan_device->swap_support = swap_support;
	vulkan_device->queue_indices = indices;

	return true;
}

static VkPhysicalDevice
select_phys_device(VkInstance inst, VkSurfaceKHR surface, VulkDevice *device)
{
	uint32_t count;
	vk_oom(vkEnumeratePhysicalDevices(inst, &count, NULL));
	if (count == 0) {
		perr("Failed to find any GPUs with Vulkan support");
		return VK_NULL_HANDLE;
	}

	VkPhysicalDevice *devices = mem_alloc(count * sizeof(*devices));
	vk_oom(vkEnumeratePhysicalDevices(inst, &count, devices));

	VkPhysicalDevice chosen = VK_NULL_HANDLE;
	for (size_t i = 0; i < count; i++) {
		if (device_meets_requirements(devices[i], surface,
					required_features, device)) {
			chosen = devices[i];
			break;
		}
	}

	mem_free(devices);

	return chosen;
}

static bool
create_device(VkPhysicalDevice phys, VulkDevice *device)
{
	uint32_t queue_ind[2] = {
		device->queue_indices.gfx,
		device->queue_indices.prs,
	};
	unsigned int index_count = (queue_ind[0] == queue_ind[1]) ? 1 : 2;

	VkDeviceQueueCreateInfo queue_infos[2];

	float queue_priority = 1.0f;
	for (size_t i = 0; i < index_count; i++) {
		queue_infos[i] = (VkDeviceQueueCreateInfo){
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.queueFamilyIndex = queue_ind[i],
			.queueCount = 1,
			.pQueuePriorities = &queue_priority,
		};
	}

	VkDeviceCreateInfo device_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pQueueCreateInfos = queue_infos,
		.queueCreateInfoCount = index_count,
		.ppEnabledExtensionNames = required_features.exts,
		.enabledExtensionCount = required_features.ext_count,
	};

	VkDevice dev;
	VkResult ret = vkCreateDevice(phys, &device_info, NULL, &dev);
	if (ret != VK_SUCCESS) {
		perr("Failed to create Vulkan device (%s)", vk_strerror(ret));
		return false;
	}

	/* NOTE: This must be changed when using multiple devices */
	volkLoadDevice(dev);

	device->handle = dev;

	vkGetDeviceQueue(dev, device->queue_indices.gfx, 0, &device->gfx_queue);
	if (index_count == 2)
		vkGetDeviceQueue(dev, device->queue_indices.prs, 0, &device->prs_queue);
	else if (index_count == 1)
		device->prs_queue = device->gfx_queue;

	return true;
}

bool
vulk_device_create(VulkDevice *device, VkInstance inst, VkSurfaceKHR surface)
{
	VkPhysicalDevice physical = select_phys_device(inst, surface, device);
	if (!physical) {
		perr("Failed to find a suitable GPU with Vulkan support");
		return false;
	}

	if (!create_device(physical, device)) {
		mem_free(device->swap_support.formats);
		mem_free(device->swap_support.pres_modes);
		return false;
	}

	device->physical = physical;
	vkGetPhysicalDeviceProperties(physical, &device->properties);

	return true;
}

void
vulk_device_destroy(VulkDevice *device)
{
	mem_free(device->swap_support.formats);
	mem_free(device->swap_support.pres_modes);
	vkDestroyDevice(device->handle, NULL);
}
