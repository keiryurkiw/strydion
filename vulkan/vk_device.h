/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_DEVICE_H
#define VK_DEVICE_H

#include "vk_include.h"

#include "../defines.h"

typedef struct {
	VkSurfaceCapabilitiesKHR capabilities;
	VkSurfaceFormatKHR *formats;
	uint32_t format_count;
	VkPresentModeKHR *pres_modes;
	uint32_t pres_mode_count;
} VulkSwapSupport;

typedef struct {
	uint32_t gfx, prs, cmp, xfr;
} VulkQFamilyIndices;

typedef struct {
	VkPhysicalDevice physical;
	VkPhysicalDeviceProperties properties;
	VulkSwapSupport swap_support;

	VkDevice handle;
	VulkQFamilyIndices queue_indices;
	VkQueue gfx_queue;
	VkQueue prs_queue;
} VulkDevice;

VulkSwapSupport vulk_device_query_swap_supp(VkPhysicalDevice device,
		VkSurfaceKHR surface);
VulkQFamilyIndices vulk_device_get_fam_indices(VkPhysicalDevice device,
		VkSurfaceKHR surface);

bool vulk_device_create(VulkDevice *device, VkInstance inst,
		VkSurfaceKHR surface);
void vulk_device_destroy(VulkDevice *device);

#endif /* VK_DEVICE_H */
