/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_INITIALIZERS_H
#define VK_INITIALIZERS_H

#include "vk_include.h"

#include "../defines.h"

#define VULK_PIPELINE_SHADER_STAGE_INFO(_stage, _module) \
	(VkPipelineShaderStageCreateInfo){ \
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, \
		.stage = _stage, \
		.module = _module, \
		.pName = "main", \
	}

#define VULK_PIPELINE_INPUT_ASSEMBLY_INFO(_topology) \
	(VkPipelineInputAssemblyStateCreateInfo){ \
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, \
		.topology = _topology, \
		.primitiveRestartEnable = VK_FALSE, \
	}

#define VULK_PIPELINE_RASTERIZATION_STATE_INFO(_polygon_mode) \
	(VkPipelineRasterizationStateCreateInfo){ \
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, \
		.depthClampEnable = VK_FALSE, \
		.rasterizerDiscardEnable = VK_FALSE, \
		.polygonMode = _polygon_mode, \
		.cullMode = VK_CULL_MODE_BACK_BIT, \
		.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE, \
		.depthBiasEnable = VK_FALSE, \
		.lineWidth = 1.0f, \
	}

#define VULK_PIPELINE_MULTISAMPLE_STATE_INFO(_samples) \
	(VkPipelineMultisampleStateCreateInfo){ \
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, \
		.rasterizationSamples = _samples, \
		.sampleShadingEnable = VK_FALSE, \
		.alphaToCoverageEnable = VK_FALSE, \
		.alphaToOneEnable = VK_FALSE, \
	}

#endif /* VK_INITIALIZERS_H */
