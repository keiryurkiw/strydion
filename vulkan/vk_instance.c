/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#define VK_INCLUDE_PLATFORM
#include "vk_include.h"

#include "../defines.h"
#include "../utils.h"
#include "../platform/utils.h"
#include "vk_instance.h"
#include "vk_utils.h"

static const char *const instance_extension_names[] = {
	VK_KHR_SURFACE_EXTENSION_NAME,
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	VK_KHR_XCB_SURFACE_EXTENSION_NAME,
#endif
#ifndef NDEBUG
	VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#endif
};

#ifndef NDEBUG
static const char *const validation_layer_names[] = {
	"VK_LAYER_KHRONOS_validation",
	"VK_LAYER_KHRONOS_synchronization2",
};

static const VkDebugUtilsMessageSeverityFlagBitsEXT debug_severity =
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;

static VkDebugUtilsMessengerEXT debug_messenger = VK_NULL_HANDLE;

static void
get_validation_layers(char **layer_names, uint32_t *layer_count)
{
	*layer_count = 0;

	uint32_t count;
	vk_oom(vkEnumerateInstanceLayerProperties(&count, NULL));
	if (count == 0)
		return;
	VkLayerProperties *props = mem_alloc(count * sizeof(*props));
	vk_oom(vkEnumerateInstanceLayerProperties(&count, props));

	const char *const *layers = validation_layer_names;
	for (size_t i = 0; i < ARR_LEN(validation_layer_names); i++) {
		bool found = false;
		for (size_t j = 0; j < count; j++) {
			if (strcmp(layers[i], props[j].layerName) == 0) {
				found = true;
				layer_names[(*layer_count)++] = (char *)layers[i];
				break;
			}
		}

		if (!found)
			pwrn("Failed to find validation layer '%s'", layers[i]);
	}

	mem_free(props);
}

static VKAPI_ATTR VkBool32 VKAPI_CALL
vulkan_debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT severity,
		VkDebugUtilsMessageTypeFlagBitsEXT type,
		const VkDebugUtilsMessengerCallbackDataEXT *data, void *usr_data)
{
	(void)usr_data;

	char *message = mem_alloc(strlen(data->pMessage)+1 + 128);
	strcpy(message, "strydion: vulkan ");

	if (type) {
		if (type & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
			strcat(message, "general|"); 
		if (type & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
			strcat(message, "validation|"); 
		if (type & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
			strcat(message, "performance|"); 
		message[strlen(message)-1] = '\0';
	}

	if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
		strcat(message, " \x1b[01;31merror:\x1b[m\n");
	if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
		strcat(message, " \x1b[01;33mwarning:\x1b[m\n");
	VkDebugUtilsMessageTypeFlagBitsEXT info =
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
	if (severity & info)
		strcat(message, " info:\n");

	if (strncmp(data->pMessage, "Validation Error: ",
				LIT_LEN("Validation Error: ")) == 0) {
		strcat(message, data->pMessage + LIT_LEN("Validation Error: "));
	} else {
		strcat(message, data->pMessage);
	}

	fprintf(stderr, "%s\n", message);

	mem_free(message);

	return VK_FALSE;
}

static void
create_vulkan_debugger(VkInstance instance)
{
	VkDebugUtilsMessengerCreateInfoEXT msgr_info = {
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
		.messageSeverity = debug_severity,
		.messageType =
			VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
		.pfnUserCallback = vulkan_debug_callback,
	};

	VkDebugUtilsMessengerEXT msgr;
	vk_oom(vkCreateDebugUtilsMessengerEXT(instance, &msgr_info, NULL, &msgr));

	debug_messenger = msgr;
}
#endif /* NDEBUG */

VkInstance
vulk_init(void)
{
	VkResult ret = volkInitialize();
	if (ret != VK_SUCCESS) {
		perr("Failed to load Vulkan");
		return VK_NULL_HANDLE;
	}

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "Strydion",
		.applicationVersion = VK_MAKE_VERSION(0, 0, 0),
		.pEngineName = "Strydion",
		.engineVersion = VK_MAKE_VERSION(0, 0, 0),
		.apiVersion = VK_MAKE_API_VERSION(0, 1, 3, 0),
	};

#ifndef NDEBUG
	char *layer_names[ARR_LEN(validation_layer_names)];
	uint32_t layer_count = 0;
	get_validation_layers(layer_names, &layer_count);
#endif

	VkInstanceCreateInfo inst_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
		.ppEnabledExtensionNames = instance_extension_names,
		.enabledExtensionCount = ARR_LEN(instance_extension_names),
#ifndef NDEBUG
		.ppEnabledLayerNames = (const char *const *)layer_names,
		.enabledLayerCount = layer_count,
#endif
	};

	VkInstance instance;
	ret = vkCreateInstance(&inst_info, NULL, &instance);
	if (ret != VK_SUCCESS) {
		perr("Failed to create a vulkan instance (%s)", vk_strerror(ret));
		return VK_NULL_HANDLE;
	}
	volkLoadInstanceOnly(instance);

#ifndef NDEBUG
	create_vulkan_debugger(instance);
#endif

	return instance;
}

void
vulk_deinit(VkInstance instance)
{
#ifndef NDEBUG
	if (debug_messenger)
		vkDestroyDebugUtilsMessengerEXT(instance, debug_messenger, NULL);
#endif
	vkDestroyInstance(instance, NULL);
}
