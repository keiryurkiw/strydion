/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cglm/types.h>
#include <string.h>
#include "vk_include.h"

#include "../defines.h"
#include "../platform/utils.h"
#include "vk_alloc.h"
#include "vk_mesh.h"
#include "vk_utils.h"

static inline VulkMeshInputDesc
get_input_description(void)
{
	return (VulkMeshInputDesc){
		.binding = {
			.binding = 0,
			.stride = sizeof(VulkVertex),
			.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
		},
		.attributes = {
			{
				.binding = 0,
				.location = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = offsetof(VulkVertex, pos),
			}, {
				.binding = 0,
				.location = 1,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = offsetof(VulkVertex, clr),
			}
		},
	};
}

VulkMesh
vulk_mesh_create_cube(VulkAllocator *allocator, VulkDevice *device)
{
#define RED    { 204.0f / 255.0f, 36.0f  / 255.0f, 29.0f  / 255.0f }
#define ORANGE { 214.0f / 255.0f, 93.0f  / 255.0f, 14.0f  / 255.0f }
#define YELLOW { 255.0f / 255.0f, 153.0f / 255.0f, 33.0f  / 255.0f }
#define GREEN  { 152.0f / 255.0f, 151.0f / 255.0f, 26.0f  / 255.0f }
#define BLUE   { 69.0f  / 255.0f, 133.0f / 255.0f, 136.0f / 255.0f }
#define PURPLE { 177.0f / 255.0f, 98.0f  / 255.0f, 134.0f / 255.0f }

	VulkVertex vertices[] = {
		{ .pos = { -1.0f, -1.0f, -1.0f }, .clr = RED },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = RED },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = RED },
		{ .pos = {  1.0f,  1.0f, -1.0f }, .clr = RED },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = RED },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = RED },

		{ .pos = { -1.0f, -1.0f, -1.0f }, .clr = ORANGE },
		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = ORANGE },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = ORANGE },
		{ .pos = { -1.0f,  1.0f,  1.0f }, .clr = ORANGE },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = ORANGE },
		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = ORANGE },

		{ .pos = {  1.0f, -1.0f,  1.0f }, .clr = YELLOW },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = YELLOW },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = YELLOW },
		{ .pos = {  1.0f,  1.0f, -1.0f }, .clr = YELLOW },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = YELLOW },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = YELLOW },

		{ .pos = { -1.0f,  1.0f,  1.0f }, .clr = GREEN },
		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = GREEN },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = GREEN },
		{ .pos = {  1.0f, -1.0f,  1.0f }, .clr = GREEN },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = GREEN },
		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = GREEN },

		{ .pos = {  1.0f,  1.0f, -1.0f }, .clr = BLUE },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = BLUE },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = BLUE },
		{ .pos = { -1.0f,  1.0f,  1.0f }, .clr = BLUE },
		{ .pos = {  1.0f,  1.0f,  1.0f }, .clr = BLUE },
		{ .pos = { -1.0f,  1.0f, -1.0f }, .clr = BLUE },

		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = PURPLE },
		{ .pos = { -1.0f, -1.0f, -1.0f }, .clr = PURPLE },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = PURPLE },
		{ .pos = { -1.0f, -1.0f,  1.0f }, .clr = PURPLE },
		{ .pos = {  1.0f, -1.0f, -1.0f }, .clr = PURPLE },
		{ .pos = {  1.0f, -1.0f,  1.0f }, .clr = PURPLE },
	};

#undef RED
#undef ORANGE
#undef YELLOW
#undef GREEN
#undef BLUE
#undef PURPLE

	VkBufferCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = sizeof(vertices),
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
	};
	VulkBuffer buffer = vulk_alloc_buffer(allocator, device, &info);

	void *ptr;
	vk_oom(vkMapMemory(device->handle, buffer.memory, 0, buffer.size, 0, &ptr));
	memcpy(ptr, vertices, sizeof(vertices));
	vkUnmapMemory(device->handle, buffer.memory);

	return (VulkMesh){
		.vertex_buffer = buffer,
		.vertex_count = sizeof(vertices) / sizeof(*vertices),
		.input_description = get_input_description(),
	};
}

void
vulk_mesh_destroy(VulkDevice *device, VulkMesh *mesh)
{
	vulk_alloc_free_buffer(device, &mesh->vertex_buffer);
}
