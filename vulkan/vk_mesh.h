/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_MESH_H
#define VK_MESH_H

#include <cglm/types.h>

#include "../defines.h"
#include "vk_device.h"
#include "vk_alloc.h"

typedef struct {
	vec3 pos;
	vec3 clr;
} VulkVertex;

typedef struct {
	VkVertexInputBindingDescription binding;
	VkVertexInputAttributeDescription attributes[2]; /* For pos and clr */
} VulkMeshInputDesc;

typedef struct {
	VulkBuffer vertex_buffer;
	size_t vertex_count;

	VulkMeshInputDesc input_description;
} VulkMesh;

VulkMesh vulk_mesh_create_triangle(VulkAllocator *allocator, VulkDevice *device);
VulkMesh vulk_mesh_create_cube(VulkAllocator *allocator, VulkDevice *device);
void vulk_mesh_destroy(VulkDevice *device, VulkMesh *mesh);

#endif /* VK_MESH_H */
