/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "vk_include.h"

#include "../defines.h"
#include "../platform/utils.h"
#include "../utils.h"
#include "vk_device.h"
#include "vk_pipeline.h"
#include "vk_swapchain.h"
#include "vk_utils.h"

VkCommandPool
vulk_pipe_create_cmd_pool(VulkDevice *device)
{
	VkCommandPoolCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = device->queue_indices.gfx,
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
	};

	VkCommandPool pool;
	vk_oom(vkCreateCommandPool(device->handle, &info, NULL, &pool));
	return pool;
}

VkCommandBuffer
vulk_pipe_alloc_cmd_buf(VulkDevice *device, VkCommandPool pool)
{
	VkCommandBufferAllocateInfo info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = pool,
		.commandBufferCount = 1,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	};

	VkCommandBuffer buffer;
	vk_oom(vkAllocateCommandBuffers(device->handle, &info, &buffer));
	return buffer;
}

VkRenderPass
vulk_pipe_create_renderpass(VulkDevice *device, VkFormat image_format)
{
	VkAttachmentDescription attachments[] = {
		{ /* Colour */
			.format = image_format,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
		}, { /* Depth */
			.format = VK_FORMAT_D32_SFLOAT,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
		},
	};

	VkAttachmentReference colour_attach_ref = {
		.attachment = 0,
		.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
	};

	VkAttachmentReference depth_attach_ref = {
		.attachment = 1,
		.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	};

	VkSubpassDescription subpass = {
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.colorAttachmentCount = 1,
		.pColorAttachments = &colour_attach_ref,
		.pDepthStencilAttachment = &depth_attach_ref,
	};

	VkSubpassDependency dependencies[] = {
		{ /* Colour */
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.dstSubpass = 0,
			.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			.srcAccessMask = 0,
			.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
		}, { /* Depth */
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.dstSubpass = 0,
			.srcStageMask =
				VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
				VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
			.srcAccessMask = 0,
			.dstStageMask =
				VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
				VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
			.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
		},
	};

	VkRenderPassCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = 2,
		.pAttachments = attachments,
		.subpassCount = 1,
		.pSubpasses = &subpass,
		.dependencyCount = 2,
		.pDependencies = dependencies,
	};

	VkRenderPass rpass;
	vk_oom(vkCreateRenderPass(device->handle, &create_info, NULL, &rpass));
	return rpass;
}

VkFence
vulk_pipe_create_fence(VulkDevice *device)
{
	VkFenceCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
	};

	VkFence fence;
	vk_oom(vkCreateFence(device->handle, &info, NULL, &fence));
	return fence;
}

VkSemaphore
vulk_pipe_create_semaphore(VulkDevice *device)
{
	VkSemaphoreCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	};

	VkSemaphore semaphore;
	vk_oom(vkCreateSemaphore(device->handle, &info, NULL, &semaphore));
	return semaphore;
}

VkShaderModule
vulk_pipe_create_shader(VulkDevice *device, char *path)
{
	File code = map_file(path);
	if (!code.data) {
		perr("Failed to create shader module");
		return VK_NULL_HANDLE;
	}
	assert(code.size % 4 == 0 && "Shader code is not valid SPIR-V");

	VkShaderModuleCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = code.size,
		.pCode = code.data,
	};

	VkShaderModule module;
	vk_oom(vkCreateShaderModule(device->handle, &info, NULL, &module));
	unmap_file(&code);
	return module;
}

VkPipeline
vulk_pipe_create(
		VulkDevice *device, VkRenderPass renderpass,
		VkPipelineShaderStageCreateInfo *shader_stages,
		uint32_t shader_stage_count,
		VkPipelineVertexInputStateCreateInfo vert_input_info,
		VkPipelineInputAssemblyStateCreateInfo assembly_info,
		VkViewport viewport, VkRect2D scissor,
		VkPipelineRasterizationStateCreateInfo rasterizer_info,
		VkPipelineColorBlendAttachmentState colour_blend_attach,
		VkPipelineMultisampleStateCreateInfo multisample_info,
		VkPipelineLayout layout
		)
{
	VkPipelineViewportStateCreateInfo viewport_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.pViewports = &viewport,
		.scissorCount = 1,
		.pScissors = &scissor,
	};

	VkPipelineColorBlendStateCreateInfo colour_blend_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.logicOpEnable = VK_FALSE,
		.attachmentCount = 1,
		.pAttachments = &colour_blend_attach,
	};

	VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		.depthTestEnable = VK_TRUE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
		.depthBoundsTestEnable = VK_FALSE,
		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
		.stencilTestEnable = VK_FALSE,
	};

	VkDynamicState dynamic_states[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR,
	};

	VkPipelineDynamicStateCreateInfo dynamic_state_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.dynamicStateCount = sizeof(dynamic_states) / sizeof(dynamic_states[0]),
		.pDynamicStates = dynamic_states,
	};

	VkGraphicsPipelineCreateInfo pipeline_info = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = shader_stage_count,
		.pStages = shader_stages,
		.pVertexInputState = &vert_input_info,
		.pInputAssemblyState = &assembly_info,
		.pViewportState = &viewport_info,
		.pRasterizationState = &rasterizer_info,
		.pMultisampleState = &multisample_info,
		.pColorBlendState = &colour_blend_info,
		.pDepthStencilState = &depth_stencil_info,
		.pDynamicState = &dynamic_state_info,
		.layout = layout,
		.renderPass = renderpass,
	};

	VkPipeline pipeline;
	vk_oom(vkCreateGraphicsPipelines(device->handle, VK_NULL_HANDLE,
			1, &pipeline_info, NULL, &pipeline));
	return pipeline;
}
