/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_PIPELINE_H
#define VK_PIPELINE_H

#include "vk_include.h"

#include "../defines.h"
#include "vk_device.h"
#include "vk_swapchain.h"

VkCommandPool vulk_pipe_create_cmd_pool(VulkDevice *device);
VkCommandBuffer vulk_pipe_alloc_cmd_buf(VulkDevice *device, VkCommandPool pool);

VkRenderPass vulk_pipe_create_renderpass(VulkDevice *device,
		VkFormat image_format);

VkFence vulk_pipe_create_fence(VulkDevice *device);
VkSemaphore vulk_pipe_create_semaphore(VulkDevice *device);

VkShaderModule vulk_pipe_create_shader(VulkDevice *device, char *path);

VkPipeline
vulk_pipe_create(VulkDevice *device, VkRenderPass renderpass,
		VkPipelineShaderStageCreateInfo *shader_stages,
		uint32_t shader_stage_count,
		VkPipelineVertexInputStateCreateInfo vert_input_info,
		VkPipelineInputAssemblyStateCreateInfo assembly_info,
		VkViewport viewport, VkRect2D scissor,
		VkPipelineRasterizationStateCreateInfo rasterizer_info,
		VkPipelineColorBlendAttachmentState colour_blend_attach,
		VkPipelineMultisampleStateCreateInfo multisample_info,
		VkPipelineLayout layout);

#endif /* VK_PIPELINE_H */
