/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "vk_include.h"

#include "../defines.h"
#include "../platform/utils.h"
#include "../platform/window.h"
#include "../utils.h"
#include "vk_alloc.h"
#include "vk_device.h"
#include "vk_swapchain.h"
#include "vk_utils.h"

#define PREF_PRES_MODE VK_PRESENT_MODE_MAILBOX_KHR
#define PREF_FORMAT VK_FORMAT_B8G8R8A8_UNORM
#define PREF_COLOURSPACE VK_COLOR_SPACE_SRGB_NONLINEAR_KHR

VkSurfaceFormatKHR
vulk_swap_get_format(VulkSwapSupport *swap_supp)
{
	for (size_t i = 0; i < swap_supp->format_count; i++) {
		VkSurfaceFormatKHR fmt = swap_supp->formats[i];
		if (fmt.format == PREF_FORMAT && fmt.colorSpace == PREF_COLOURSPACE)
			return fmt;
	}

	return swap_supp->formats[0];
}

static VkPresentModeKHR
choose_present_mode(VulkSwapSupport *swap_supp)
{
	for (size_t i = 0; i < swap_supp->pres_mode_count; i++) {
		VkPresentModeKHR mode = swap_supp->pres_modes[i];
		if (mode == PREF_PRES_MODE)
			return mode;
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

static VkExtent2D
adjust_extent(void *win, VulkSwapSupport *swap_supp)
{
	VkSurfaceCapabilitiesKHR *caps = &swap_supp->capabilities;
	if (caps->currentExtent.width != UINT32_MAX)
		return caps->currentExtent;

	uint32_t w, h;
	win_get_fb_size(win, &w, &h);

	w = CLAMP(w, caps->minImageExtent.width, caps->maxImageExtent.width);
	h = CLAMP(h, caps->minImageExtent.height, caps->maxImageExtent.height);

	return (VkExtent2D){
		.width = w,
		.height = h,
	};
}

static inline uint32_t
get_image_count(VulkDevice *device, VkSwapchainKHR swapchain)
{
	uint32_t count;
	vk_oom(vkGetSwapchainImagesKHR(device->handle, swapchain, &count, NULL));
	return count;
}

static void
get_swap_objs(VulkDevice *device, VkRenderPass rpass,
		VulkSwapchain *swapchain)
{
	uint32_t count = swapchain->objects.count;
	vk_oom(vkGetSwapchainImagesKHR(device->handle, swapchain->handle, &count,
				swapchain->objects.images));

	for (size_t i = 0; i < count; i++) {
		VkImageViewCreateInfo view_info = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = swapchain->objects.images[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = swapchain->surf_fmt.format,
			.components = (VkComponentMapping){
				.r = VK_COMPONENT_SWIZZLE_IDENTITY,
				.g = VK_COMPONENT_SWIZZLE_IDENTITY,
				.b = VK_COMPONENT_SWIZZLE_IDENTITY,
				.a = VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			.subresourceRange = (VkImageSubresourceRange){
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

		VkResult ret = vkCreateImageView(device->handle, &view_info, NULL,
				&swapchain->objects.image_views[i]);
		vk_oom(ret);
		if (ret != VK_SUCCESS) {
			perr("Failed to create swapchain image view with index %zu (%s)",
					i, vk_strerror(ret));
		}

		VkImageView attachments[] = {
			swapchain->objects.image_views[i],
			swapchain->depth_image_view,
		};

		VkFramebufferCreateInfo fb_info = {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.renderPass = rpass,
			.attachmentCount = 2,
			.pAttachments = attachments,
			.width = swapchain->extent.width,
			.height = swapchain->extent.height,
			.layers = 1,
		};

		vk_oom(vkCreateFramebuffer(device->handle, &fb_info, NULL,
					&swapchain->objects.framebuffers[i]));
	}
}

bool
vulk_swap_create(void *win, VulkAllocator *allocator, VulkDevice *device,
		VkSurfaceKHR surface, VkRenderPass renderpass, VulkSwapchain *swapchain)
{
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device->physical, surface,
			&device->swap_support.capabilities);

	VkSurfaceCapabilitiesKHR *caps = &device->swap_support.capabilities;

	/* Prefer triple-buffering but fallback on min or max if required. */
	uint32_t image_count = MAX(caps->minImageCount, 3u);
	if (caps->maxImageCount > 0)
		image_count = MIN(image_count, caps->maxImageCount);

	VkSurfaceFormatKHR format = vulk_swap_get_format(&device->swap_support);
	VkExtent2D extent = adjust_extent(win, &device->swap_support);
	VkPresentModeKHR mode = choose_present_mode(&device->swap_support);
	VkSwapchainCreateInfoKHR info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = surface,
		.minImageCount = image_count,
		.imageFormat = format.format,
		.imageColorSpace = format.colorSpace,
		.imageExtent = extent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.preTransform = caps->currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = mode,
		.clipped = VK_TRUE,
	};

	VulkQFamilyIndices *indices = &device->queue_indices;
	if (indices->gfx != indices->prs) {
		info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		info.queueFamilyIndexCount = 2;
		info.pQueueFamilyIndices = (uint32_t *)indices;
	}

	VkSwapchainKHR sc;
	VkResult ret = vkCreateSwapchainKHR(device->handle, &info, NULL, &sc);
	vk_oom(ret);
	if (ret != VK_SUCCESS) {
		perr("Failed to create a swapchain (%s)", vk_strerror(ret));
		return false;
	}

	swapchain->handle = sc;
	swapchain->extent = extent;
	swapchain->surf_fmt = format;
	swapchain->present_mode = mode;

	image_count = get_image_count(device, sc);
	swapchain->objects = (VulkSwapObjs){
		.images = mem_alloc(image_count * sizeof(VkImage)),
		.image_views = mem_alloc(image_count * sizeof(VkImageView)),
		.framebuffers = mem_alloc(image_count * sizeof(VkFramebuffer)),
		.count = image_count,
	};

	VkImageCreateInfo depth_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_D32_SFLOAT,
		.extent = {
			extent.width,
			extent.height,
			1,
		},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
	};
	swapchain->depth_image = vulk_alloc_image(allocator, device, &depth_info);

	VkImageViewCreateInfo depth_view_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.image = swapchain->depth_image.handle,
		.format = VK_FORMAT_D32_SFLOAT,
		.subresourceRange = {
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
			.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
		},
	};
	vk_oom(vkCreateImageView(device->handle, &depth_view_info,
				NULL, &swapchain->depth_image_view));

	get_swap_objs(device, renderpass, swapchain);

	return true;
}

void
vulk_swap_destroy(VulkDevice *device, VulkSwapchain *swapchain)
{
	vkDestroyImageView(device->handle, swapchain->depth_image_view, NULL);
	vulk_alloc_free_image(device, &swapchain->depth_image);

	for (size_t i = 0; i < swapchain->objects.count; i++) {
		vkDestroyImageView(device->handle, swapchain->objects.image_views[i],
				NULL);
		vkDestroyFramebuffer(device->handle, swapchain->objects.framebuffers[i],
				NULL);
	}
	mem_free(swapchain->objects.framebuffers);
	mem_free(swapchain->objects.image_views);
	mem_free(swapchain->objects.images);

	vkDestroySwapchainKHR(device->handle, swapchain->handle, NULL);
}

bool
vulk_swap_recreate(void *win, VulkAllocator *allocator, VulkDevice *device,
		VkSurfaceKHR surface, VkRenderPass renderpass, VulkSwapchain *swapchain)
{
	vkDeviceWaitIdle(device->handle);
	vulk_swap_destroy(device, swapchain);
	return vulk_swap_create(win, allocator, device,
			surface, renderpass, swapchain);
}
