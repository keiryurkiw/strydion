/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_SWAPCHAIN_H
#define VK_SWAPCHAIN_H

#include "vk_include.h"

#include "../defines.h"
#include "vk_alloc.h"
#include "vk_device.h"

typedef struct {
	VkImage *images;
	VkImageView *image_views;
	VkFramebuffer *framebuffers;
	uint32_t count;
} VulkSwapObjs;

typedef struct {
	VkSurfaceFormatKHR surf_fmt;
	VkPresentModeKHR present_mode;
	VkExtent2D extent;
	VkSwapchainKHR handle;

	VulkSwapObjs objects;

	VulkImage depth_image;
	VkImageView depth_image_view;
} VulkSwapchain;

VkSurfaceFormatKHR vulk_swap_get_format(VulkSwapSupport *swap_supp);

bool vulk_swap_create(void *win, VulkAllocator *allocator, VulkDevice *device,
		VkSurfaceKHR surface, VkRenderPass renderpass,
		VulkSwapchain *swapchain);
void vulk_swap_destroy(VulkDevice *device, VulkSwapchain *swapchain);
bool vulk_swap_recreate(void *win, VulkAllocator *allocator, VulkDevice *device,
		VkSurfaceKHR surface, VkRenderPass renderpass,
		VulkSwapchain *swapchain);

#endif /* VK_SWAPCHAIN_H */
