/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of Strydion.
 *
 * Strydion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Strydion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Strydion.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VK_UTILS_H
#define VK_UTILS_H

#include "vk_include.h"

#include "../defines.h"

#define VK_ASSERT(_ret) assert(_ret == VK_SUCCESS)

char *vk_strerror(VkResult res);

/* Used for checking if a Vulkan function returns an out of memory error.
 * Similar to the allocation functions, we'll just abort() for now. */
void vk_oom(VkResult res);

#endif /* VK_UTILS_H */
